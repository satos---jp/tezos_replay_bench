open Re

type level = Level of int
type milli_second = MSec of float

let parse_time =
  let ms = compile @@ Pcre.re {|^([0-9.]+)ms$|} in
  let sec = compile @@ Pcre.re {|^([0-9.]+)s$|} in
  let mins = compile @@ Pcre.re {|^([0-9.]+)min(([0-9.]+)s)?$|} in
  fun s ->
    match exec_opt ms s with
    | Some g -> MSec (float_of_string (Group.get g 1))
    | None -> (
        match exec_opt sec s with
        | Some g -> MSec (float_of_string (Group.get g 1) *. 1000.0)
        | None -> (
            match exec_opt mins s with
            | Some g -> MSec begin
                (float_of_string (Group.get g 1) *. 60.0)
                +. (try float_of_string (Group.get g 3) with Not_found -> 0.0)
                   *. 1000.0
              end
            | None -> failwith s))

let parse ic =
  let tbl = Hashtbl.create 1000 in
  let block = compile @@ Pcre.re {|replaying block .* \(([0-9]+)\)|} in
  let time = compile @@ Pcre.re {|block validated in ([^ ]+)$|} in
  let rec loop () =
    match input_line ic with
    | exception End_of_file -> tbl
    | l1 -> (
        match exec_opt block l1 with
        | None -> loop ()
        | Some g ->
            let level = Group.get g 1 in
            let rec loop2 () =
              match input_line ic with
              | exception End_of_file -> assert false
              | l2 -> (
                  match exec_opt time l2 with
                  | None -> loop2 ()
                  | Some g ->
                      let tim = Group.get g 1 in
                      let ms = parse_time tim in
                      Hashtbl.add tbl (Level (int_of_string level)) ms ;
                      loop ())
            in
            loop2 ())
  in
  loop ()

let parse_file fn =
  let ic = open_in fn in
  let res = parse ic in
  close_in ic ;
  res

let percentile compare p fs =
  let fs = List.sort compare fs in
  let len = List.length fs in
  let th = int_of_float @@ float len *. p in
  List.nth fs th

let write_file filename f =
  let out = open_out filename in
  match
    try Ok (f out) with e -> Error e
  with
  | Ok y -> close_out out; y
  | Error e -> close_out out; raise e

let create_csv data out =
  let fmt = Format.formatter_of_out_channel out in
  let normal =
    List.sort (fun (l1, _) (l2, _) -> compare l1 l2)
    @@ List.of_seq @@ Hashtbl.to_seq data
  in
  Format.fprintf fmt "level, normal@.";
  List.iter
    (fun (Level level, MSec ms_normal) ->
      Format.fprintf fmt "%d, %f@." level ms_normal)
    normal

let create_avarages data out =
  let fmt = Format.formatter_of_out_channel out in
  Format.fprintf fmt "|  |  |@.";
  Format.fprintf fmt "| --- | --- |@.";
  let normal =
    List.sort (fun (l1, _) (l2, _) -> compare l1 l2)
    @@ List.of_seq @@ Hashtbl.to_seq data
  in

  let snd_ms (_, MSec ms) = ms in
  let worst xs = List.hd @@ List.sort (fun x y -> - compare x y) (List.map snd_ms xs) in
  let average xs = List.fold_left (+.) 0.0 (List.map snd_ms xs) /. float (List.length xs) in

  let w1 = worst normal in
  Format.fprintf fmt "|worst |  %.2f|@." w1;

  let f p =
    let f1 = percentile compare p (List.map snd_ms normal) in
    Format.fprintf fmt "|%.02f percentile |  %.2f |@." (p *. 100.0)
      f1
  in
  f 0.995;
  f 0.99;
  f 0.9;
  f 0.8;
  f 0.7;
  f 0.6;
  f 0.5;

  let a1 = average normal in
  Format.fprintf fmt "|average |  %.2f |@." a1

let () =
  if Array.length Sys.argv <> 3 then begin
    Format.printf "Usage: dune exec src/parse_replay.exe -- <replay_log_file> <report_dir>@.";
    failwith "illegal command line parameters"
  end else begin
    let replay_log_file = Sys.argv.(1) in
    let report_dir = Sys.argv.(2) in
    let csvfile = report_dir ^ "/replays.csv" in
    let avaragefile = report_dir ^ "/percentiles.md" in
    let data = parse_file replay_log_file in
    write_file csvfile (create_csv data);
    write_file avaragefile (create_avarages data);
  end
