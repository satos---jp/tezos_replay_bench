open Re

type level = Level of int
type milli_second = MSec of float
type gc_stats = {major_collections: int;
                 compactions: int;
                 live_words_bytes: int32; }
type stats = {
  replay_time_ms: float;
  mem_rss_bytes: int32;
  disk_usage_gb: float;
}

let bytes_of_words i =
  match Sys.word_size with
  | 64 -> Int32.(mul 8l (of_int i))
  | 32 -> Int32.(mul 4l (of_int i))
  | _ -> assert false

let pp_gigabyte fmt i32 =
  Format.fprintf fmt "%f" (Int32.to_float i32 /. 1000_000_000.)

let parse_time =
  let ms = compile @@ Pcre.re {|^([0-9.]+)ms$|} in
  let sec = compile @@ Pcre.re {|^([0-9.]+)s$|} in
  let mins = compile @@ Pcre.re {|^([0-9.]+)min(([0-9.]+)s)?$|} in
  fun s ->
    match exec_opt ms s with
    | Some g -> MSec (float_of_string (Group.get g 1))
    | None -> (
        match exec_opt sec s with
        | Some g -> MSec (float_of_string (Group.get g 1) *. 1000.0)
        | None -> (
            match exec_opt mins s with
            | Some g -> MSec begin
                (float_of_string (Group.get g 1) *. 60.0)
                +. (try float_of_string (Group.get g 3) with Not_found -> 0.0)
                   *. 1000.0
              end
            | None -> failwith s))

let parse_line_or_skip re ic =
  let rec loop () =
    let line = input_line ic in
    match exec_opt re line with
    | None -> loop ()
    | Some g -> g
  in
  loop ()

(*
Oct  4 17:21:04.970 - node.replay: replaying block BMW8SnJ57KMcmSVimBhPhNP1YPYSGEJARoNRg3tdW1YScCMfBh6 (3)
...
Oct  4 17:21:04.994 - context: disk: 0 replaydir-1/context 21816
Oct  4 17:21:04.996 - context: Memory stats: level=0: Statm {page_size: 4096; size:61363; resident: 41314; shared: 13524; text: 16651; lib: 0; data: 38388; dt: 0}
...
Oct  4 17:21:04.996 - node.replay: block validated in 25.810ms
*)
let parse ic =
  let tbl = Hashtbl.create 1000 in
  let block = compile @@ Pcre.re {|replaying block .* \(([0-9]+)\)|} in
  let disk = compile @@ Pcre.re {|disk: .* ([0-9]+)|} in
  let mem = compile @@ Pcre.re {|Memory .*page_size: ([0-9]+);.* resident: ([0-9]+); .*|} in
  let time = compile @@ Pcre.re {|block validated in ([^ ]+)$|} in
  let rec loop () =
    match parse_line_or_skip block ic with
    | exception End_of_file -> tbl
    | g1 ->
      let level = Group.get g1 1 in
      let g2 = parse_line_or_skip disk ic in
      let disk_usage_gb = (float_of_string @@ Group.get g2 1) /. 1000_000. in
      let g3 = parse_line_or_skip mem ic in
      let page_size = int_of_string @@ Group.get g3 1 in
      let mem_rss = Int32.of_string @@ Group.get g3 2 in
      let mem_rss_bytes = Int32.mul (Int32.of_int page_size) mem_rss in
      let g4 = parse_line_or_skip time ic in
      let tim = Group.get g4 1 in
      let MSec replay_time_ms = parse_time tim in
      let stats = {replay_time_ms; mem_rss_bytes; disk_usage_gb} in
      Hashtbl.add tbl (Level (int_of_string level)) stats;
      loop ()
  in
  loop ()

let parse_file fn =
  let ic = open_in fn in
  let res = parse ic in
  close_in ic ;
  res

let percentile compare p fs =
  let fs = List.sort compare fs in
  let len = List.length fs in
  let th = int_of_float @@ float len *. p in
  List.nth fs th

let write_file filename f =
  let out = open_out filename in
  match
    try Ok (f out) with e -> Error e
  with
  | Ok y -> close_out out; y
  | Error e -> close_out out; raise e

let create_csv data out =
  let fmt = Format.formatter_of_out_channel out in
  let normal =
    List.sort (fun (l1, _) (l2, _) -> compare l1 l2)
    @@ List.of_seq @@ Hashtbl.to_seq data
  in
  Format.fprintf fmt "#level, time[ms], RSS[bytes], Max RSS[bytes], Disk@.";
  List.fold_left
  (fun prev_max_rss (Level level, stats) ->
    let max_rss = max stats.mem_rss_bytes prev_max_rss in
    Format.fprintf fmt "%d, %f, %a, %a, %f@."
      level stats.replay_time_ms pp_gigabyte stats.mem_rss_bytes pp_gigabyte max_rss
      stats.disk_usage_gb;
    max_rss)
    Int32.min_int normal
  |> ignore

let create_avarages data out =
  let fmt = Format.formatter_of_out_channel out in
  Format.fprintf fmt "|     | [msec] |@.";
  Format.fprintf fmt "| --- | --- |@.";
  let normal =
    List.sort (fun (l1, _) (l2, _) -> compare l1 l2)
    @@ List.map (fun (l, stats) -> (l, stats.replay_time_ms))
    @@ List.of_seq @@ Hashtbl.to_seq data
  in

  let worst xs = List.hd @@ List.sort (fun x y -> - compare x y) (List.map snd xs) in
  let average xs = List.fold_left (+.) 0.0 (List.map snd xs) /. float (List.length xs) in

  let w1 = worst normal in
  Format.fprintf fmt "|worst |  %.2f|@." w1;

  let f p =
    let f1 = percentile compare p (List.map snd normal) in
    Format.fprintf fmt "|%.02f percentile |  %.2f |@." (p *. 100.0)
      f1
  in
  f 0.995;
  f 0.99;
  f 0.9;
  f 0.8;
  f 0.7;
  f 0.6;
  f 0.5;

  let a1 = average normal in
  Format.fprintf fmt "|average |  %.2f |@." a1

let () =
  if Array.length Sys.argv <> 3 then begin
    Format.printf "Usage: dune exec src/parse_replay.exe -- <replay_log_file> <report_dir>@.";
    failwith "illegal command line parameters"
  end else begin
    let replay_log_file = Sys.argv.(1) in
    let report_dir = Sys.argv.(2) in
    let csvfile = report_dir ^ "/replays.csv" in
    let avaragefile = report_dir ^ "/percentiles.md" in
    let data = parse_file replay_log_file in
    write_file csvfile (create_csv data);
    write_file avaragefile (create_avarages data);
  end
