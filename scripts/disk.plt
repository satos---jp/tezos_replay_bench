set datafile separator ","

set term svg
set output report_dir."/disk.svg"

set xlabel 'Block Level'
set ylabel 'Giga Byte'

set format x "%gk"

plot report_dir."/replays.csv" using ($1/1000):5 with lines linecolor rgb "dark-violet" title "Disk Usage"
