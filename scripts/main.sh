#!/bin/sh
set -eux

DATE=$(date)
COMMAND="$0 $@"

## options
CREATE_FRAMEGRAPH="false"
while getopts "f" optchar; do
    case $optchar in
        f) CREATE_FRAMEGRAPH="true" ;;
    esac
done
shift $(expr $OPTIND - 1)

## parameters
ARCHIVE_DIR=$1
SNAPSHOT_DIR=$2
TEZOS_REPOSITORY=$3
TEZOS_COMMIT_HASH=$4
LEVEL1=$5
LEVEL2=$6

if [ ! -e $ARCHIVE_DIR ]; then
    echo "Not found: $ARCHIVE_DIR"
    exit 1
fi

DIR=$(cd $(dirname "$0")/.. && pwd)
TARGET_BLOCKS=$(seq -s " " $((LEVEL1 + 1)) $LEVEL2)
CONTEXT_VERSION=$(cat $ARCHIVE_DIR/version.json | jq -r ".version")
SNAPSHOT=$SNAPSHOT_DIR/snapshot-$CONTEXT_VERSION-$LEVEL1
TEZOS_SRC_DIR=$DIR/tezos-$TEZOS_COMMIT_HASH
IMPORT_DIR=importdir-$LEVEL1
REPLAY_DIR=replaydir-$LEVEL1
REPORT_DIR=$DIR/reports/report-$TEZOS_COMMIT_HASH-$LEVEL1-$LEVEL2
LOG_OUTPUT=$REPORT_DIR/node.log

if [ ! -e $TEZOS_SRC_DIR ]; then
    git clone $TEZOS_REPOSITORY $TEZOS_SRC_DIR
    cd $TEZOS_SRC_DIR/
    git checkout $TEZOS_COMMIT_HASH
    NINJA_REPOSITORY=https://gitlab.com/proof_ninja/tezos.git
    git remote add ninja $NINJA_REPOSITORY
    git fetch ninja
    git cherry-pick --no-commit 146630d7
    make build-deps
    eval $(opam config env)
    make
else
    cd $TEZOS_SRC_DIR/
fi

## create snapshot of block level $LEVEL1
if [ ! -e $SNAPSHOT ]; then
    ./tezos-node snapshot export --data-dir $ARCHIVE_DIR $SNAPSHOT --block $LEVEL1
fi

## create context which have only a $LEVEL1 block
if [ ! -e $IMPORT_DIR ]; then
    mkdir $IMPORT_DIR
    ./tezos-node snapshot import $SNAPSHOT --data-dir $IMPORT_DIR
fi

## prepare for replaying
if [ ! -e $REPLAY_DIR ]; then
    mkdir $REPLAY_DIR
    cp -a $ARCHIVE_DIR/store $REPLAY_DIR/
    cp $ARCHIVE_DIR/*.json $REPLAY_DIR/
    cp -a $IMPORT_DIR/context $REPLAY_DIR/
    ./tezos-node config reset --data-dir $REPLAY_DIR
else
    rm -rf $REPLAY_DIR/context
    cp -a $IMPORT_DIR/context $REPLAY_DIR
fi

rm -rf $REPORT_DIR
mkdir -p $REPORT_DIR
echo $COMMAND > $REPORT_DIR/command
echo $DATE > $REPORT_DIR/date

du -s $REPLAY_DIR/context | awk '{print $1}' > $REPORT_DIR/context_size_kb_before

## do replay
export TEZOS_LOG='* -> debug'
if [ $CREATE_FRAMEGRAPH = "true" ]; then
    $DIR/scripts/create_flamegraph.sh $ARCHIVE_DIR $TEZOS_COMMIT_HASH $LEVEL1 $LEVEL2
else
    /usr/bin/time -o $REPORT_DIR/time_sec --format "%e" ./tezos-node replay --data-dir $REPLAY_DIR $TARGET_BLOCKS > $LOG_OUTPUT 2>&1
fi

du -s $REPLAY_DIR/context | awk '{print $1}' > $REPORT_DIR/context_size_kb_after

## analyze replay log
cd $DIR
dune exec src/parse_replay_with_memstats.exe $LOG_OUTPUT $REPORT_DIR

## create report
dune exec src/make_report.exe -- $REPORT_DIR
gnuplot -e "report_dir='$REPORT_DIR'" scripts/rss.plt
gnuplot -e "report_dir='$REPORT_DIR'" scripts/disk.plt
